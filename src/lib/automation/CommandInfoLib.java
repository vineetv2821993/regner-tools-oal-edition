package lib.automation;

public class CommandInfoLib {
	
    public enum CommandType { Connect, MasterVolume, MasterVolumeMuteToggle, Brightness, BrightnessScreenToggle, CDRom, Clipboard, Resolution, Keyboard, Touchpad, TouchpadButton, TouchpadWheel, Console, ConsoleTool, SystemBasic, ExecutePlayer, VoiceBot, Speak}
    public static class CommandValue
    {
        //CommandType = MasterVolume, Brightness, Touchpad, TouchpadWheel having Dynamic CommandValue

        public class Connect
        {
            //CommandType = Connect
            public final static String CONNECT = "Connecting";
        }
        public class Console
        {
            public final static String NULL = "";
            //CommandType = Console
            public class ConsoleTool {
                public final static String EXECUTE_PLAYER = "exec max \"C:\\Program Files\\Windows Media Player\\wmplayer.exe\"";
                public final static String EXECUTE_CONNECTED_TRAY = "trayballoon \"AirVoice Server\" \"Client is Connected\" \"shell32.dll,22\" 15000";
                public final static String EXECUTE_VIDEO_STREAMER = "exec hide VideoStreamer.exe\"";
            }
           
            public final static String KILL_PLAYER = "taskkill /f /im wmplayer.exe";
        }
        public class MediaPlayer
        {
            //CommandType = Keyboard
            public final static String PLAY = "ctrl+p";
            public final static String STOP = "ctrl+s";
            public final static String PREVIOUS = "ctrl+f";
            public final static String NEXT = "ctrl+b";
            public final static String SLOW = "ctrl+shift+s";
            public final static String NORMAL = "ctrl+shift+n";
            public final static String FAST = "ctrl+shift+g";
            public final static String SUFFLE = "ctrl+h";
            public final static String REPEAT = "ctrl+t";
            public final static String PLAYER_VOLUME_UP = "F9";
            public final static String PLAYER_VOLUME_DOWN = "F8";
        }

        public class PowerPoint
        {
            //CommandType = Keyboard
            public final static String NEXT_SLIDE = "pagedown";
            public final static String PREVIOUS_SLIDE = "pageup";
            public final static String JUMP_EVENT = "enter";
            //Powerpoint number+JUMP_EVENT e.g to Jump to Page 11, "1+1+enter"
            public final static String ZOOM_IN = "ctrl+plus";
            public final static String ZOOM_OUT = "ctrl+minus";
            public final static String FULLSCREEN = "F5";
            public final static String PRESENTER_VIEW = "alt+F5";
            public final static String EXIT_FULLSCREEN = "esc";
        }
        public class TouchpadButton
        {
            //CommandType = TouchpadButton
            public final static String LEFT_CLICK = "left click";
            public final static String RIGHT_CLICK = "right click";
            public final static String MIDDLE_CLICK = "middle click";
            public final static String LEFT_DBL_CLICK = "left dblclick";
            public final static String RIGHT_DBL_CLICK = "right dblclcik";
            public final static String MIDDLE_DBL_CLICK = "middle dbl click";
        }

        public class SystemBasic
        {
            //CommandType = SystemBasic
            public final static String SHUTDOWN = "exitwin shutdown";
            public final static String POWER_OFF = "exitwin poweroff";
            public final static String RESTART = "exitwin reboot";
            public final static String LOG_OFF = "exitwin logoff";
            public final static String HIBERNATE = "hibernate";
            public final static String SCREENSAVER = "screensaver";
            public final static String SHUTDOWN_FORCE = "exitwin shutdown force";
            public final static String POWER_OFF_FORCE = "exitwin poweroff force";
            public final static String RESTART_FORCE = "exitwin reboot force";
            public final static String LOG_OFF_FORCE = "exitwin logoff forceifhung";
            public final static String HIBERNATE_FORCE = "hibernate force";

            public final static String MINIMIZE_ALL = "rwin+m";
            public final static String CLOSE_CURRENT_WINDOW = "alt+F4";

            public class MasterVolumeMuteToggle
            {
                //CommandType = MasterVolumeMuteToggle
                public final static String MUTE = "mutesysvolume 1";
                public final static String UNMUTE = "mutesysvolume 0";
            }
            public class BrightnessScreenToggle
            {
                //CommandType = BrightnessScreenToggle
                public final static String SCREEN_TURN_OFF = "monitor off";
                public final static String SCREEN_TURN_ON = "monitor on";
            }
            public class CDRom
            {
                //CommandType = CDRom
                public final static String OPEN = "cdrom open";
                public final static String CLOSE = "cdrom close";
            }

            public class Resolution
            {
                //CommandType = Resolution
                public final static String RES_800_600_16 = "setdisplay 800 600 16";
                public final static String RES_800_600_32 = "setdisplay 800 600 32";
                public final static String RES_1024_768_16 = "setdisplay 1024 768 16";
                public final static String RES_1024_768_32 = "setdisplay 1024 768 32";
                public final static String RES_1280_720_32 = "setdisplay 1280 720 32";
                public final static String RES_1366_768_32 = "setdisplay 1366 768 32";
                public final static String RES_1920_1080_32 = "setdisplay 1920 1080 32";

                public final String[] RES = new String [] {
                    "setdisplay 800 600 16",
			        "setdisplay 800 600 32",
			        "setdisplay 1024 768 16",
			        "setdisplay 1024 768 32",
			        "setdisplay 1280 720 32",
			        "setdisplay 1366 768 32",
                    "setdisplay 1920 1080 32"
                };
            }

            public class Clipboard
            {
                //CommandType = Clipboard
                public final static String SPEAK = "speak text ~$clipboard$";
                public final static String CLEAR = "clipboard clear";
            }
        }
        public static final String[] VoiceSet = new String [] {
        		"open drive",
        		"brightness maximum",
        		"brightness minimum",
        		"brightness medium",
        		"maximum volume",
        		"medium volume",
        		"minimum volume",
        		"hibernate",
        		"shut down",
        		"sleep mode",
        		"screen on",
        		"next",
        		"back",
        		"maximize",
        		"exit"
        };
        public static class VoiceCommandData
        {
        	public static boolean isValidData(String msg){
        		for(int i=0; i< VoiceSet.length; i++){
        			if(VoiceSet[i].equals(msg)) return true;
        		}
        		return false;
        	}

        }
    }

}
