package lib.automation;

import lib.automation.CommandInfoLib.CommandType;


public class VoiceMapper {
	public static String toString(CommandType CT, String CommandValue){
		return CT.name()+"."+CommandValue;
	}

	public static String VoiceAnalyzer(String args){
		if(args.equals("open drive")){
			return toString(CommandInfoLib.CommandType.CDRom,CommandInfoLib.CommandValue.SystemBasic.CDRom.OPEN);
		}
		if(args.equals("brightness maximum")){
			return toString(CommandInfoLib.CommandType.Brightness, String.valueOf(100));
		}
		if(args.equals("brightness minimum")){
			return toString(CommandInfoLib.CommandType.Brightness, String.valueOf(0));
		}
		if(args.equals("brightness medium")){
			return toString(CommandInfoLib.CommandType.Brightness, String.valueOf(50));
		}
		if(args.equals("maximum volume")){
			return toString(CommandInfoLib.CommandType.MasterVolume,String.valueOf(65535));
		}
		if(args.equals("medium volume")){
			return toString(CommandInfoLib.CommandType.MasterVolume, String.valueOf(65535/2));
		}
		if(args.equals("minimum volume")){
			return toString(CommandInfoLib.CommandType.MasterVolume, String.valueOf(0));
		}
		if(args.equals("hibernate")){
			return toString(CommandInfoLib.CommandType.SystemBasic, CommandInfoLib.CommandValue.SystemBasic.HIBERNATE);
		}
		if(args.equals("shut down")){
			return toString(CommandInfoLib.CommandType.SystemBasic, CommandInfoLib.CommandValue.SystemBasic.SHUTDOWN);
		}
		if(args.equals("sleep mode")){
			return toString(CommandInfoLib.CommandType.SystemBasic, CommandInfoLib.CommandValue.SystemBasic.BrightnessScreenToggle.SCREEN_TURN_OFF);
		}
		if(args.equals("screen on")){
			return toString(CommandInfoLib.CommandType.SystemBasic, CommandInfoLib.CommandValue.SystemBasic.BrightnessScreenToggle.SCREEN_TURN_ON);
		}
		if(args.equals("next")){
			return toString(CommandInfoLib.CommandType.Keyboard,CommandInfoLib.CommandValue.PowerPoint.NEXT_SLIDE);
		}
		if(args.equals("back")){
			return toString(CommandInfoLib.CommandType.Keyboard,CommandInfoLib.CommandValue.PowerPoint.PREVIOUS_SLIDE);
		}
		if(args.equals("maximize")){
			return toString(CommandInfoLib.CommandType.Keyboard,CommandInfoLib.CommandValue.PowerPoint.FULLSCREEN);
		}
		if(args.equals("exit")){
			return toString(CommandInfoLib.CommandType.Keyboard,CommandInfoLib.CommandValue.PowerPoint.EXIT_FULLSCREEN);
		}
		
		return "";
	}

}
