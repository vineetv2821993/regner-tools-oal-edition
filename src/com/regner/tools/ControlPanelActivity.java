package com.regner.tools;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import lib.automation.CommandInfoLib;
import lib.automation.CommandInfoLib.CommandType;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.Button;
import android.widget.Toast;
import android.widget.ZoomControls;

public class ControlPanelActivity extends Activity {
	
	public void SendTCPRequest(CommandType CT, String CommandValue){
		ClientThread.setCommand(CT.name()+"."+CommandValue);
		ClientThread.myThread = new Thread(new ClientThread());
		ClientThread.myThread.start();
	}
	
	public static ImageButton bMax,bComp, bClose, bPen, bText, bPlay, bRight, bLeft, bMouse;
	public static Button bWMP, bNP; 
	public static boolean bMaxToggle,bCompToggle, bCloseToggle, bPenToggle, bTextToggle, bPlayToggle, bRightToggle, bLeftToggle, bMouseToggle;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
        WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_control_panel);
	    // The "loadAdOnCreate" and "testDevices" XML attributes no longer available.
	    AdView adView = (AdView) this.findViewById(R.id.adViewApp);
	    AdRequest adRequest = new AdRequest.Builder()
	        .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
	        .build();
	    adView.loadAd(adRequest);
		initializeButtonSequence();
	}
	public void initializeButtonSequence(){

		Toast.makeText(getBaseContext(), ClientThread.SERVER_IP, Toast.LENGTH_LONG).show();
		bComp  = (ImageButton) findViewById(R.id.imageButtonComp);
	    bCompToggle = true;
		bComp.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub	
				if(bCompToggle == true){
				bComp.setImageResource(R.drawable.computerglow);
				bCompToggle = false;
				}
				else{
					bComp.setImageResource(R.drawable.computer);
					bCompToggle = true;
				}				
				Intent NextActivity = new Intent("com.regner.tools.TouchScreenActivity");
				startActivity(NextActivity);
				

			}
		});		
		bRight  = (ImageButton) findViewById(R.id.imageButtonRight);
	    bRightToggle = true;
		bRight.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				SendTCPRequest(CommandInfoLib.CommandType.Keyboard,CommandInfoLib.CommandValue.PowerPoint.NEXT_SLIDE);
					
			}
		});	
		bLeft  = (ImageButton) findViewById(R.id.imageButtonLeft);
	    bLeftToggle = true;
		bLeft.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				SendTCPRequest(CommandInfoLib.CommandType.Keyboard,CommandInfoLib.CommandValue.PowerPoint.PREVIOUS_SLIDE);
			}
		});	
		bPlay  = (ImageButton) findViewById(R.id.imageButtonPlay);
	    bPlayToggle = true;
		bPlay.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				SendTCPRequest(CommandInfoLib.CommandType.Keyboard,CommandInfoLib.CommandValue.PowerPoint.PRESENTER_VIEW);
			}
		});	
		bMax  = (ImageButton) findViewById(R.id.imageButtonMax);
	    bMaxToggle = true;
		bMax.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				SendTCPRequest(CommandInfoLib.CommandType.Keyboard,CommandInfoLib.CommandValue.PowerPoint.FULLSCREEN);
			}
		});	
		bClose  = (ImageButton) findViewById(R.id.imageButtonClose);
	    bCloseToggle = true;
		bClose.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				SendTCPRequest(CommandInfoLib.CommandType.Keyboard,CommandInfoLib.CommandValue.PowerPoint.EXIT_FULLSCREEN);
			}
		});	
		ZoomControls slideZoom = (ZoomControls) findViewById(R.id.zoomControl);
		slideZoom.setOnZoomInClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				SendTCPRequest(CommandInfoLib.CommandType.Keyboard,CommandInfoLib.CommandValue.PowerPoint.ZOOM_IN);
				
			}
		});
		slideZoom.setOnZoomOutClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				SendTCPRequest(CommandInfoLib.CommandType.Keyboard,CommandInfoLib.CommandValue.PowerPoint.ZOOM_OUT);
			}
		});

	}

}
