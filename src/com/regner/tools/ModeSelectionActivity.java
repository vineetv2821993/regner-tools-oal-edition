package com.regner.tools;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.RadioButton;

public class ModeSelectionActivity extends Activity {

	private Button doneButton;
	private RadioButton radioBInternet, radioBWiFi, radioBHotSpot;
	private final String MODE_INTERNET = "com.regner.tools.LoginActivity";
	private final String MODE_WIFI = "com.regner.tools.IPInfo";
	private String SetIntent;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mode_selection);
		init();
	}

	private void init() {
		radioBInternet = (RadioButton) findViewById(R.id.radioButtonInternet);
		radioBWiFi = (RadioButton) findViewById(R.id.radioButtonWiFi);
		radioBHotSpot = (RadioButton) findViewById(R.id.radioButtonHotSpot);
		radioBInternet.setOnCheckedChangeListener(new OnCheckedChangeListener(){

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				if(isChecked == true){
					radioBWiFi.setChecked(false);
					radioBHotSpot.setChecked(false);
					SetIntent = MODE_INTERNET;
				}
			}}
		);
		radioBWiFi.setOnCheckedChangeListener(new OnCheckedChangeListener(){

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				if(isChecked == true){
					radioBInternet.setChecked(false);
					radioBHotSpot.setChecked(false);
					SetIntent = MODE_WIFI;
				}
				
			}}
		);
		radioBHotSpot.setOnCheckedChangeListener(new OnCheckedChangeListener(){

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				if(isChecked == true){
					radioBWiFi.setChecked(false);
					radioBInternet.setChecked(false);
					SetIntent = MODE_WIFI;
				}
			}}
		);
		doneButton = (Button) findViewById(R.id.buttonDone);
		doneButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(SetIntent));
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.mode_selection, menu);
		return true;
	}

}
