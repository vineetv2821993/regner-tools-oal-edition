package com.regner.tools;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

class ClientThread implements Runnable {
	private Socket socket;
	public static final int SERVERPORT = 12128;
	//private static final int RESULT_CLOSE_ALL = 1000;
	public static String SERVER_IP;
	//private static String Command;
	public static Thread myThread;
	private static String Command;
	public static String ServerText = "768 1366";
	public static void setCommand(String cmd){
		Command = cmd;
	}
	public static String getCommand(){
		return Command;
	}
	
	  static void copy(InputStream in, OutputStream out) throws IOException {
	      byte[] buf = new byte[8192];
	      int len = 0;
	      while ((len = in.read(buf)) != -1) {
	          out.write(buf, 0, len);
	      }
	  }


	@Override
	public void run() {
		try {
			InetAddress serverAddr = InetAddress.getByName(SERVER_IP);
			socket = new Socket(serverAddr, SERVERPORT);
			PrintWriter out = new PrintWriter(new BufferedWriter(
					new OutputStreamWriter(socket.getOutputStream())),
					true);
			out.println(getCommand());
			
			BufferedReader bf = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			ServerText = bf.readLine();
			socket.close();


		} catch (UnknownHostException e1) {
			//Toast.makeText(getBaseContext(), "ErrorD: "+e1.toString(), Toast.LENGTH_LONG).show();
			e1.printStackTrace();
		} catch (IOException e1) {
			//Toast.makeText(getBaseContext(), "ErrorE: "+e1.toString(), Toast.LENGTH_LONG).show();
			e1.printStackTrace();
		}

	}

}