package com.regner.tools;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import lib.automation.CommandInfoLib;
import lib.automation.CommandInfoLib.CommandType;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.ToggleButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.SeekBar.OnSeekBarChangeListener;

public class PlayerWMPActivity extends Activity {
	
	public static ImageButton bStart, bRepeat, bSuffle;
	public static boolean bStartToggle, bSuffleToggle = true, bRepeatToggle = true, speedChange = false;

	public static int initProgress = 0;
	public void SendTCPRequest(CommandType CT, String CommandValue){
		ClientThread.setCommand(CT.name()+"."+CommandValue);
		ClientThread.myThread = new Thread(new ClientThread());
		ClientThread.myThread.start();
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
	    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
	    WindowManager.LayoutParams.FLAG_FULLSCREEN);
		SendTCPRequest(CommandInfoLib.CommandType.ExecutePlayer,CommandInfoLib.CommandValue.Console.NULL);
		setContentView(R.layout.wmp_player);
	    // The "loadAdOnCreate" and "testDevices" XML attributes no longer available.
	    AdView adView = (AdView) this.findViewById(R.id.adViewApp);
	    AdRequest adRequest = new AdRequest.Builder()
	        .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
	        .build();
	    adView.loadAd(adRequest);
		initializeMediaPlayerControls();
	}

	public void initializeMediaPlayerControls(){
		
		//Master Control

		SeekBar audioSeekBar = (SeekBar) findViewById(R.id.seekBarMaster);
		audioSeekBar.setMax(65535);
		OnSeekBarChangeListener audioChangeListener = new OnSeekBarChangeListener(){

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				SendTCPRequest(CommandInfoLib.CommandType.MasterVolume,String.valueOf(progress));
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub

			}

		};
		audioSeekBar.setOnSeekBarChangeListener(audioChangeListener);
		
		//VLC Control
		

		SeekBar audioSeekBarVLC = (SeekBar) findViewById(R.id.seekBarWMP);
		audioSeekBarVLC.setMax(10);

		OnSeekBarChangeListener audioChangeListenerVLC = new OnSeekBarChangeListener(){

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				int progD = progress - initProgress;
				int loopN = 0;
				String cmd = "";
				if(progress == 0){
					loopN=16;
					cmd=CommandInfoLib.CommandValue.MediaPlayer.PLAYER_VOLUME_DOWN;
				}
				else if(progress == 10){
					loopN=10;
					cmd=CommandInfoLib.CommandValue.MediaPlayer.PLAYER_VOLUME_UP;
				}
				else if(progD>0){
					loopN=progD;
					cmd=CommandInfoLib.CommandValue.MediaPlayer.PLAYER_VOLUME_UP;
				}
				else if(progD<0){
					loopN=-progD;
					cmd=CommandInfoLib.CommandValue.MediaPlayer.PLAYER_VOLUME_DOWN;
				}
				// TODO Auto-generated method stub
				
				for(int i=0; i<loopN;i++){
					SendTCPRequest(CommandInfoLib.CommandType.Keyboard,cmd);
				}
				initProgress = progress;
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub

			}

		};
		audioSeekBarVLC.setOnSeekBarChangeListener(audioChangeListenerVLC);
		
		//Mute Toggle
		ToggleButton muteToggleButton = (ToggleButton) findViewById(R.id.toggleButtonMaster);
		OnCheckedChangeListener muteListener = new OnCheckedChangeListener(){

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub
				if(isChecked)
					SendTCPRequest(CommandInfoLib.CommandType.MasterVolumeMuteToggle,CommandInfoLib.CommandValue.SystemBasic.MasterVolumeMuteToggle.MUTE);
				else
					SendTCPRequest(CommandInfoLib.CommandType.MasterVolumeMuteToggle,CommandInfoLib.CommandValue.SystemBasic.MasterVolumeMuteToggle.UNMUTE);

			}};
			muteToggleButton.setOnCheckedChangeListener(muteListener);
		//VLC Toggle
			ToggleButton muteToggleButtonVLC = (ToggleButton) findViewById(R.id.toggleButtonWMP);
			OnCheckedChangeListener muteListenerVLC = new OnCheckedChangeListener(){

				@Override
				public void onCheckedChanged(CompoundButton buttonView,
						boolean isChecked) {
					// TODO Auto-generated method stub
					if(isChecked)
					{
						SendTCPRequest(CommandInfoLib.CommandType.Keyboard, "F7");
					} 
					else{
						SendTCPRequest(CommandInfoLib.CommandType.Keyboard, "F7");
					}
				}};
				muteToggleButtonVLC.setOnCheckedChangeListener(muteListenerVLC);
		//START STOP REPLAY
				bStart  = (ImageButton) findViewById(R.id.imageButtonPlay);
			    bStartToggle = true;
				bStart.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						SendTCPRequest(CommandInfoLib.CommandType.Keyboard, CommandInfoLib.CommandValue.MediaPlayer.PLAY);	
						if(bStartToggle == true){
						bStart.setImageResource(R.drawable.pause);
						bStartToggle = false;
						}
						else{
							bStart.setImageResource(R.drawable.play);
							bStartToggle = true;
						}
					}
				});
				ImageButton bStop  = (ImageButton) findViewById(R.id.imageButtonStop);
				bStop.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						SendTCPRequest(CommandInfoLib.CommandType.Keyboard, CommandInfoLib.CommandValue.MediaPlayer.STOP);
						speedChange = true;
					}
				});

				ImageButton bFast  = (ImageButton) findViewById(R.id.imageButtonFast);
				bFast.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if(speedChange==true){
							SendTCPRequest(CommandInfoLib.CommandType.Keyboard, CommandInfoLib.CommandValue.MediaPlayer.NORMAL);
								speedChange = false;
						}else{	
							SendTCPRequest(CommandInfoLib.CommandType.Keyboard, CommandInfoLib.CommandValue.MediaPlayer.FAST);
						speedChange = true;
						}
					}
				});
				ImageButton bSlow  = (ImageButton) findViewById(R.id.imageButtonSlow);
				bSlow.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						
						// TODO Auto-generated method stub
						if(speedChange==true){
							SendTCPRequest(CommandInfoLib.CommandType.Keyboard, CommandInfoLib.CommandValue.MediaPlayer.NORMAL);
						speedChange = false;
						}else{
							SendTCPRequest(CommandInfoLib.CommandType.Keyboard, CommandInfoLib.CommandValue.MediaPlayer.SLOW);
						speedChange = true;
						}
					}
				});

				bSuffle  = (ImageButton) findViewById(R.id.imageButtonSuffle);
				bSuffle.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						// TODO Auto-generated method stub
						SendTCPRequest(CommandInfoLib.CommandType.Keyboard, CommandInfoLib.CommandValue.MediaPlayer.SUFFLE);
						if(bSuffleToggle == true){
						bSuffle.setImageResource(R.drawable.suffleon);
						bSuffleToggle = false;
						}
						else{
							bSuffle.setImageResource(R.drawable.suffleoff);
							bSuffleToggle = true;
						}
					}
				});
				//Repeat Toggle
				bRepeat  = (ImageButton) findViewById(R.id.imageButtonRepeat);
				bRepeat.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						// TODO Auto-generated method stub
						SendTCPRequest(CommandInfoLib.CommandType.Keyboard, CommandInfoLib.CommandValue.MediaPlayer.REPEAT);	
						if(bRepeatToggle == true){
						bRepeat.setImageResource(R.drawable.repeaton);
						bRepeatToggle = false;
						}
						else{
							bRepeat.setImageResource(R.drawable.repeatoff);
							bRepeatToggle = true;
						}
					}
				});
					//quitVLC	
						Button quitVLC  = (Button) findViewById(R.id.buttonQuitWMP);
						quitVLC.setOnClickListener(new View.OnClickListener() {
							
							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								// TODO Auto-generated method stub
								SendTCPRequest(CommandInfoLib.CommandType.Console, CommandInfoLib.CommandValue.Console.KILL_PLAYER);	
							}
						});

						ImageButton bFWD = (ImageButton) findViewById(R.id.imageButtonNext); 
						bFWD.setOnClickListener(new View.OnClickListener() {

							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								SendTCPRequest(CommandInfoLib.CommandType.Keyboard, CommandInfoLib.CommandValue.MediaPlayer.NEXT);


							}
						});
						ImageButton bBCK = (ImageButton) findViewById(R.id.imageButtonPrev); 
						bBCK.setOnClickListener(new View.OnClickListener() {

							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								SendTCPRequest(CommandInfoLib.CommandType.Keyboard, CommandInfoLib.CommandValue.MediaPlayer.PREVIOUS);

							}
						});
						Button bBring = (Button) findViewById(R.id.buttonBringWMPF); 
						bBring.setOnClickListener(new View.OnClickListener() {

							@Override
							public void onClick(View v) {
								SendTCPRequest(CommandInfoLib.CommandType.ExecutePlayer,CommandInfoLib.CommandValue.Console.NULL);
							}
						});

	}

}
