 package com.regner.tools;

import java.util.ArrayList;
import java.util.Locale;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import lib.automation.CommandInfoLib;
import lib.automation.CommandInfoLib.CommandType;
import lib.automation.VoiceMapper;


import android.os.Bundle;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.Engine;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
	public void SendTCPRequest(CommandType CT, String CommandValue){
		ClientThread.setCommand(CT.name()+"."+CommandValue);
		ClientThread.myThread = new Thread(new ClientThread());
		ClientThread.myThread.start();
	}
	public void SendTCPRequest(String Command){
		ClientThread.setCommand(Command);
		ClientThread.myThread = new Thread(new ClientThread());
		ClientThread.myThread.start();
	}
	protected static final int RESULT_SPEECH = 1;
	private ImageButton btnSpeak, btnTTS;

	private TextToSpeech tts;
	private static int TTS_DATA_CHECK = 2;
	private boolean isTTSInitialized = false;
	private boolean speakTextEnabled = true;
	public static ImageButton  bOpenPlayer, bOpenPPT, bScreen, bShutdown, bLogoff, bCD, bBright, bAudio,bBrightD,bHibernate,bResolution,bSleep;
	public static boolean  bScreenToggle, bShutdownToggle, bLogoffToggle, bCDToggle, bBrightToggle, bAudioToggle, bBrightDToggle,bHibernateToggle,bResolutionToggle,bSleepToggle;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
        WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_main_f);
	    // The "loadAdOnCreate" and "testDevices" XML attributes no longer available.
	    AdView adView = (AdView) this.findViewById(R.id.adViewApp);
	    AdRequest adRequest = new AdRequest.Builder()
	        .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
	        .build();
	    adView.loadAd(adRequest);
		SendTCPRequest(CommandInfoLib.CommandType.Connect,CommandInfoLib.CommandValue.Connect.CONNECT);
		confirmTTSData();
		buttonInitSequence();
	}
    private void confirmTTSData()  {
    	Intent intent = new Intent(Engine.ACTION_CHECK_TTS_DATA);
    	startActivityForResult(intent, TTS_DATA_CHECK);
    }
	private void buttonInitSequence() {
		//Toast.makeText(getBaseContext(), ClientThread.SERVER_IP, Toast.LENGTH_LONG).show();
		bScreen  = (ImageButton) findViewById(R.id.imageButtonScreen);
	    bScreenToggle = true;
		bScreen.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub	
				speakUSLocale("Screen Mirror");
				try {
					Thread.sleep(1000l);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Intent NextActivity = new Intent("com.regner.tools.TouchScreenActivity");
				startActivity(NextActivity);
				
				if(bScreenToggle == true){
				bScreen.setImageResource(R.drawable.b1);
				bScreenToggle = false;
				}
				else{
					bScreen.setImageResource(R.drawable.button1);
					bScreenToggle = true;
				}
			}
		});		
		
		bAudio  = (ImageButton) findViewById(R.id.imageButtonAudio);
	    bAudioToggle = true;
		bAudio.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if(bAudioToggle == true){
					bAudio.setImageResource(R.drawable.b8a);
					bAudioToggle = false;
					SendTCPRequest(CommandInfoLib.CommandType.MasterVolumeMuteToggle, CommandInfoLib.CommandValue.SystemBasic.MasterVolumeMuteToggle.MUTE);
					speakUSLocale("Master Volume Mute");	
				}
				else{
					bAudio.setImageResource(R.drawable.button8a);
					bAudioToggle = true;
					SendTCPRequest(CommandInfoLib.CommandType.MasterVolumeMuteToggle, CommandInfoLib.CommandValue.SystemBasic.MasterVolumeMuteToggle.UNMUTE);
					speakUSLocale("Master Volume Un Mute");					
				}
			}
		});		
		bBright  = (ImageButton) findViewById(R.id.imageButtonBright);
	    bBrightToggle = true;
		bBright.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				
				if(bBrightToggle == true){
					bBright.setImageResource(R.drawable.b7a);
					bBrightToggle = false;
				
					SendTCPRequest(CommandInfoLib.CommandType.Brightness, String.valueOf(50));
					speakUSLocale("Brightness Level Fifty");
				}
				else{
					bBright.setImageResource(R.drawable.button7a);
					bBrightToggle = true;
					SendTCPRequest(CommandInfoLib.CommandType.Brightness, String.valueOf(75));
					speakUSLocale("Brightness Level Seventy Five");
				}
			}
		});		
		bBrightD  = (ImageButton) findViewById(R.id.imageButtonBrightD);
	    bBrightDToggle = true;
		bBrightD.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if(bBrightDToggle == true){
				bBrightD.setImageResource(R.drawable.bright);
				bBrightDToggle = false;
				SendTCPRequest(CommandInfoLib.CommandType.Brightness, String.valueOf(100));
				speakUSLocale("Brightness Level Maximum");
				}
				else{
					bBrightD.setImageResource(R.drawable.brightlow);
					bBrightDToggle = true;
					SendTCPRequest(CommandInfoLib.CommandType.Brightness, String.valueOf(0));
					speakUSLocale("Brightness Level Minimum");
				}
			}
		});
		bCD  = (ImageButton) findViewById(R.id.imageButtonCD);
	    bCDToggle = true;
		bCD.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(bCDToggle == true){
				bCD.setImageResource(R.drawable.b2);
				bCDToggle = false;
				SendTCPRequest(CommandInfoLib.CommandType.CDRom, CommandInfoLib.CommandValue.SystemBasic.CDRom.OPEN);
				speakUSLocale("C D ROM ejected");
				}
				else{
					bCD.setImageResource(R.drawable.button2);
					bCDToggle = true;
					SendTCPRequest(CommandInfoLib.CommandType.CDRom, CommandInfoLib.CommandValue.SystemBasic.CDRom.CLOSE);
					speakUSLocale("C D ROM inserted");
				}
			}
		});		
		
		bLogoff  = (ImageButton) findViewById(R.id.imageButtonLogOff);
	    bLogoffToggle = true;
		bLogoff.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				speakUSLocale("Log Off Device");
				/*
	
				*/
				if(bLogoffToggle == true){
					bLogoff.setImageResource(R.drawable.b13);
					bLogoffToggle = false;
					SendTCPRequest(CommandInfoLib.CommandType.SystemBasic, CommandInfoLib.CommandValue.SystemBasic.LOG_OFF);
				}
				else{
					bLogoff.setImageResource(R.drawable.button13);
					bLogoffToggle = true;
					SendTCPRequest(CommandInfoLib.CommandType.SystemBasic, CommandInfoLib.CommandValue.SystemBasic.LOG_OFF_FORCE);
				}
			}
		});		

		
		bShutdown  = (ImageButton) findViewById(R.id.imageButtonShutdown);
	    bShutdownToggle = true;
		bShutdown.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				speakUSLocale("Shutdown Device");
				if(bShutdownToggle == true){
				bShutdown.setImageResource(R.drawable.b12);
				bShutdownToggle = false;
				SendTCPRequest(CommandInfoLib.CommandType.SystemBasic, CommandInfoLib.CommandValue.SystemBasic.SHUTDOWN);
				}
				else{
					bShutdown.setImageResource(R.drawable.button12);
					bShutdownToggle = true;
					SendTCPRequest(CommandInfoLib.CommandType.SystemBasic, CommandInfoLib.CommandValue.SystemBasic.SHUTDOWN_FORCE);
				}
			}
		});		
		
		bSleep  = (ImageButton) findViewById(R.id.imageButtonSleep);
	    bSleepToggle = true;
		bSleep.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*

				*/
				if(bSleepToggle == true){
				bSleep.setImageResource(R.drawable.sleep);
				bSleepToggle = false;
				SendTCPRequest(CommandInfoLib.CommandType.BrightnessScreenToggle, CommandInfoLib.CommandValue.SystemBasic.BrightnessScreenToggle.SCREEN_TURN_ON);
				speakUSLocale("Sleep Mode Deactivated");
				}
				else{
					bSleep.setImageResource(R.drawable.sleepglow);
					SendTCPRequest(CommandInfoLib.CommandType.BrightnessScreenToggle, CommandInfoLib.CommandValue.SystemBasic.BrightnessScreenToggle.SCREEN_TURN_OFF);
					speakUSLocale("Sleep Mode Activated");
					bSleepToggle = true;
				}
			}
		});		
		
		bHibernate  = (ImageButton) findViewById(R.id.imageButtonHibernate);
	    bHibernateToggle = true;
		bHibernate.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				speakUSLocale("System Hibernating");

				if(bHibernateToggle == true){
				bHibernate.setImageResource(R.drawable.hibernate);
				bHibernateToggle = false;
				SendTCPRequest(CommandInfoLib.CommandType.SystemBasic, CommandInfoLib.CommandValue.SystemBasic.HIBERNATE);
				}
				else{
					bHibernate.setImageResource(R.drawable.hibernateglow);
					bHibernateToggle = true;
					SendTCPRequest(CommandInfoLib.CommandType.SystemBasic, CommandInfoLib.CommandValue.SystemBasic.HIBERNATE_FORCE);
				}
			}
		});		

		bResolution  = (ImageButton) findViewById(R.id.imageButtonResolution);
	    bResolutionToggle = true;
		bResolution.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				speakUSLocale("Resolution Changed");
				/*

				*/
				if(bResolutionToggle == true){
				bResolution.setImageResource(R.drawable.resolution);
				bResolutionToggle = false;
				SendTCPRequest(CommandInfoLib.CommandType.Resolution, CommandInfoLib.CommandValue.SystemBasic.Resolution.RES_1024_768_32);
				
				}
				else{
					bResolution.setImageResource(R.drawable.resolutionglow);
					bResolutionToggle = true;
					SendTCPRequest(CommandInfoLib.CommandType.Resolution, CommandInfoLib.CommandValue.SystemBasic.Resolution.RES_1366_768_32);
				}
			}
		});		
		btnSpeak = (ImageButton) findViewById(R.id.btnSpeak);

		btnSpeak.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent intent = new Intent(
						RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
				//intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
				//intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "en-US");

				try {
					startActivityForResult(intent, RESULT_SPEECH);
					//txtText.setText("");
					Toast t = Toast.makeText(getApplicationContext(),
							"Getting Voice Command Input",
							Toast.LENGTH_SHORT);
					t.show();
				} catch (ActivityNotFoundException a) {
					Toast t = Toast.makeText(getApplicationContext(),
							"Ops! Your device doesn't support Speech to Text",
							Toast.LENGTH_SHORT);
					t.show();
				}
			}
		});
		
		btnTTS = (ImageButton) findViewById(R.id.btnTTS);

		btnTTS.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if(speakTextEnabled == true){
					speakUSLocale("Voice Navigation Disabled");
					btnTTS.setImageResource(R.drawable.imagespeakd);
					speakTextEnabled = false;
				
				}
				else{
					btnTTS.setImageResource(R.drawable.imagespeak);
					speakTextEnabled = true;
				}

			}
		});
		bOpenPlayer  = (ImageButton) findViewById(R.id.openPlayer);
		bOpenPlayer.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent NextActivity = new Intent("com.regner.tools.PlayerWMPActivity");
				startActivity(NextActivity);
			}
		});		
		bOpenPPT  = (ImageButton) findViewById(R.id.openPPT);
		bOpenPPT.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent NextActivity = new Intent("com.regner.tools.ControlPanelActivity");
				startActivity(NextActivity);
			}
		});
		
		
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		switch (requestCode) {
		case RESULT_SPEECH: {
			if (resultCode == RESULT_OK && null != data) {

				ArrayList<String> text = data
						.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

				//txtText.setText(text.get(0));
				Toast t = Toast.makeText(getApplicationContext(),
						text.get(0),
						Toast.LENGTH_SHORT);
				String msg = text.get(0).toString().trim();
				if(CommandInfoLib.CommandValue.VoiceCommandData.isValidData(msg)==true){
					SendTCPRequest(VoiceMapper.VoiceAnalyzer(msg));
				}
				else{
					SendTCPRequest(CommandInfoLib.CommandType.VoiceBot,msg);
				}
				t.show();
			}
			break;
		}

		}
    	if (requestCode == TTS_DATA_CHECK) {
    		if (resultCode == Engine.CHECK_VOICE_DATA_PASS) {
    			//Voice data exists		
    			initializeTTS();
    		}
    		else {
    			Intent installIntent = new Intent(Engine.ACTION_INSTALL_TTS_DATA);
    			startActivity(installIntent);
    		}
    	}
    	
	}
    
    private void initializeTTS() {
    	tts = new TextToSpeech(this, new OnInitListener() {
    		public void onInit(int status) {
    			if (status == TextToSpeech.SUCCESS) {
    				isTTSInitialized = true;
    			}
    			else {
    				//Handle initialization error here
    				isTTSInitialized = false;
    			}
    		}
    	});
    }
    
    
    private void speakUSLocale(String message) {
    	if(speakTextEnabled  == true){
    		if(isTTSInitialized) {
    			if (tts.isLanguageAvailable(Locale.US) >= 0) 
    				tts.setLanguage(Locale.US);
    		
    			tts.setPitch(0.8f);
    			tts.setSpeechRate(0.9f);
    		
    			tts.speak(message, TextToSpeech.QUEUE_ADD, null);
    		}
    		//else  confirmTTSData();
    	}
    }
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
    @Override
    public void onDestroy() {
    	if (tts != null) {
    		tts.stop();
    		tts.shutdown();
    	}
    	super.onDestroy();
    }

}
